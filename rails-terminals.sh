# For this script to work when not invoked directly from a shell using ./rails-terminals.sh you need to add
# eval "$BASH_POST_RC"
# at the end of ~/.bashrc
# Usage: ./rails-terminals.sh name-of-rails-project-folder
gnome-terminal --geometry 120x30 \
--tab --title 'Rails Server' --working-directory $1 -e 'bash -c "export BASH_POST_RC=\"rails server\"; exec bash"' \
--tab --title 'Rails Console' --working-directory $1 -e 'bash -c "export BASH_POST_RC=\"rails console\"; exec bash"' \
--tab --title 'Git' --working-directory $1 -e 'bash -c "export BASH_POST_RC=\"git status\"; exec bash"' \
--tab --title 'Shell' --working-directory $1 \