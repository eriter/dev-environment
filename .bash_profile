alias sublime='sublime-text-2'
alias ll='ls -al'
alias r='printf "\033c"'
alias g='git'

# Cabal executables
if [ -d "$HOME/.cabal/bin" ] ; then
    PATH="$HOME/.cabal/bin:$PATH"
fi

# A custom colored prompt!
function custom_prompt {
  # regular colors
  local K="\[\033[0;30m\]"  # black
  local R="\[\033[0;31m\]"  # red
  local G="\[\033[0;32m\]"  # green
  local Y="\[\033[0;33m\]"  # yellow
  local B="\[\033[0;34m\]"  # blue
  local M="\[\033[0;35m\]"  # magenta
  local C="\[\033[0;36m\]"  # cyan
  local W="\[\033[0;37m\]"  # white

  # emphasized (bolded) colors
  local EMK="\[\033[1;30m\]"
  local EMR="\[\033[1;31m\]"
  local EMG="\[\033[1;32m\]"
  local EMY="\[\033[1;33m\]"
  local EMB="\[\033[1;34m\]"
  local EMM="\[\033[1;35m\]"
  local EMC="\[\033[1;36m\]"
  local EMW="\[\033[1;37m\]"

  local RESET="\[\033[0m\]"

  #PS1="$EMY\u$RESET at $EMB\H$RESET in $EMG\w$RESET\$(git_status) ⚡ "  # With host
  PS1="$EMY\u$RESET $EMG\w$RESET$R\$(git_status)$RESET ⚡ "   # Without hsot
  PS2=' → '
}

function git_status {
  if [ -n "$(parse_git_branch)" ]; then
    echo -e " $(parse_git_branch)($(parse_git_commit))"
  fi
}

# Returns the branch name
function parse_git_branch {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}

# Returns the first 7 hex digits of the commit hash
function parse_git_commit {
  local commit=$(git rev-parse HEAD 2> /dev/null)
  echo ${commit:0:7}
}

custom_prompt
