Driver
  1. To install NVIDIA drivers
    For the system to recognize the added nouveau blacklist you have to run:
    sudo update-initramfs -u

  2. If drivers are broken after an update
    sudo apt-get install linux-headers-generic linux-headers-$(uname -r)

    then reinstall drivers
    sudo service light dm stop
    sudo ./NVIDIA ... --uninstall
    sudo ./NVIDIA ...

    http://askubuntu.com/questions/37590/nvidia-drivers-not-working-after-upgrade-why-can-i-only-see-terminal

    To maybe automate this process:
      http://ubuntuforums.org/showthread.php?t=835573

  3. Running NVIDIA settings as root (so that you can save the configuration)
     sudo nvidia-settings

Mouse acceleration:
  Find out the id of all devices by xinput --list
  When you've found the correct device use xinput --list-props to find all properties
  that can be changed.

  Once you find something related to mouse acceleration change this prop by:
    xinput set-prop 10 265 -1 (example)

    10 is the device id, 265 is the property "Device Accel Profile" and -1 is the value

  To make this run on startup:
    Create a file in your home directory with the xinput set-prop... in and chmod it to 766
    run gnome-session-properties in the terminal, add this file to startup commands

